import React from 'react';
import ReactDOM from 'react-dom';
import App from './pages/App';
import SignIn from './pages/SignIn';
import SignUp from './pages/SignUp';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Provider } from 'react-redux';
import store from './store';

ReactDOM.render(<Router className="App">
  <Provider store={store}>
    <Route exact={true} path="/" component={App} />
    <Route path="/sign-in" component={SignIn} />
    <Route path="/sign-up" component={SignUp} />
  </Provider>
</Router>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
