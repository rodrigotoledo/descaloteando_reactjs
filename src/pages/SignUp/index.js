import React from 'react';
import SectionHeader from '../../components/SectionHeader';

import './style.css';

import { BrowserRouter as Router, Link } from "react-router-dom";

const SignUp = (props) => (
  <div>
    <SectionHeader />
    <section className="section">
      <div className="container has-text-centered">
        <div className="column is-4 is-offset-4">
            <h3 className="title has-text-grey">Faça agora seu cadastro</h3>
            <p className="subtitle has-text-grey">Para acessar o sistema é necessário ter seu cadastro feito com as informações abaixo</p>
            <div className="box">
              <div className="field">
                <div className="control">
                  <input className="input is-normal" type="text" placeholder="Digite seu nome completo" />
                </div>
              </div>

              <div className="field">
                <div className="control">
                  <input className="input is-normal" type="email" placeholder="Digite seu email" />
                </div>
              </div>

              <div className="field">
                <div className="control">
                  <input className="input is-normal" type="password" placeholder="Digite sua senha" />
                </div>
              </div>
              <div className="buttons has-addons is-centered">
                <Link to="/" className="button is-link">
                  Quero me cadastrar
                </Link>
                <Link to="/sign-in" className="button is-link">
                  Já tenho acesso
                </Link>
              </div>
            </div>
        </div>
    </div>
    </section>
  </div>
)

export default SignUp;