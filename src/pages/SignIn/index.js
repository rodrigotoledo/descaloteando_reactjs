import React from 'react';
import SectionHeader from '../../components/SectionHeader';

import './style.css';

import { BrowserRouter as Router, Link } from "react-router-dom";

const SignIn = (props) => (
  <div>
    <SectionHeader />
    <section className="section">
      <div className="container has-text-centered">
        <div className="column is-4 is-offset-4">
            <h3 className="title has-text-grey">Entre com seus dados</h3>
            <p className="subtitle has-text-grey">Para acessar o sistema preencha as informações</p>
            <div className="box">
              <div className="field">
                <div className="control">
                  <input className="input is-normal" type="email" placeholder="Digite seu email" />
                </div>
              </div>

              <div className="field">
                <div className="control">
                  <input className="input is-normal" type="password" placeholder="Digite sua senha" />
                </div>
              </div>
              <div className="buttons has-addons is-centered">
                <Link to="/" className="button is-link">
                  Acessar sistema
                </Link>
                <Link to="/sign-up" className="button is-link">
                  Faça seu cadastro
                </Link>
              </div>
            </div>
        </div>
    </div>
    </section>
  </div>
)

export default SignIn;