import React from 'react';

import 'react-bulma-components/dist/react-bulma-components.min.css';
import './style.css';

// import api from '../../services/api';
import Menu from '../../components/Menu';
import SectionHeader from '../../components/SectionHeader';
import ChargeForm from '../../components/ChargeForm';
import UserForm from '../../components/UserForm';
import ChargesInfo from '../../components/ChargesInfo';
import SectionCharges from '../../components/SectionCharges';
import Footer from '../../components/Footer';

import { connect } from 'react-redux';

const mapStateToProps = stateReducer => ({
  pendingCharges: stateReducer["pendingCharges"],
  paidCharges: stateReducer["paidCharges"]
});

const App = ({ pendingCharges, paidCharges }) => (
  <div>
    <Menu />
    <SectionHeader />
    <section className="section" id="about">
      <div className="section-heading">
        <h3 className="title is-2">Números atuais {pendingCharges.length + paidCharges.length}  </h3>
        <h4 className="subtitle is-5">Criei, atualize e tenha total controle sobre suas cobranças</h4>
      </div>
      <div className="columns has-same-height is-gapless">
        <div className="column">
          <div className="card">
            <div className="card-content has-background-white-ter" id="charge_form">
            <ChargeForm />
            </div>
          </div>
        </div>
        <div className="column">
          <div className="card">
            <div className="card-content" id='create_charge'>
            <UserForm />
            </div>
          </div>
        </div>
        <div className="column">
          <div className="card">
            <div className="card-content has-background-white-ter skills-content" id="charges_informations">
              <ChargesInfo />
            </div>
          </div>
        </div>
      </div>
    </section>
    <div className="pending_charges">
      <SectionCharges charges={pendingCharges} title="Cobranças atrasadas" subtitle="que estão pendentes" />
    </div>
    <div className="paid_charges">
      <SectionCharges charges={paidCharges} title="Cobranças em dia" subtitle="que foram quitadas" />
    </div>
    <Footer />
  </div>
);

export default connect(mapStateToProps)(App);
