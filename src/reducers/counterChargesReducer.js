// Reducer para contagem de cobrancas
const initialCounterChargesData = {
  twoWeeksAgo: 10,
  twoWeeksMore: 15
}

export default function counterCharges(state = initialCounterChargesData, action) {
  switch (action.type) {
    default:
      return state
  }
}