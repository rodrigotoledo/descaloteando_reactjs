// Reducer de Lista de Charges/Cobranças PAGAS
const initialPendingChargesData =[
  {
    "id":5265,
    "who_to_charge":"Antônio Mendes Veloso",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5266,
    "who_to_charge":"Antônio Mendes Veloso9381bdf2-4aa5-4bfb-8794-8624180590cc",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5267,
    "who_to_charge":"Antônio Mendes Veloso3553465a-abc5-4002-88c8-1997b301b92f",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-2049fee94ed-c5a4-434e-a46f-0eff1bb4f906",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.a4497c15-2b2e-4470-aea2-ed29d5ebbdf7",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5272,
    "who_to_charge":"Antônio Mendes Veloso42e84baf-c9d4-4ae6-ac54-10d8b0ea95bf",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-20481c7dd4d-7d48-4b1d-bf5d-c09a8b313879",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.d1ea3b19-2f6e-47b6-a1b7-880b78392d62",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5273,
    "who_to_charge":"Antônio Mendes Veloso9a8e1700-d542-48b0-a47c-67af722a2f72",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204498e4ee2-fb39-4ebc-9122-9b9e63b204bb",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.9fc951fe-cf9a-4f47-9a79-95d427aea249",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5274,
    "who_to_charge":"Antônio Mendes Veloso1a5a5d07-8789-4695-ba4c-68b81f53f314",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204e89d0170-f6f1-45fc-8bb6-7d67341afb73",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.4ecdadbf-eb18-436c-9a44-f256732b5252",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5275,
    "who_to_charge":"Antônio Mendes Velosoe3a7fa08-969c-47c8-805b-6cf6dc7cf9aa",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-2043495de03-f1c3-46e3-8d0b-ba5cefa86f41",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.6559af4b-1a42-4814-b9f5-ecd0ffcb5e2e",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5276,
    "who_to_charge":"Antônio Mendes Veloso1d1f06c2-2a9e-4f05-b335-2960393b691f",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204a1940206-0f46-4e34-8a84-bfb447e0d67c",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.2f2b4962-ef48-4875-9150-075f77be8257",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5277,
    "who_to_charge":"Antônio Mendes Veloso2efc7886-2599-474b-8b74-2b4974fb6b0a",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204d45b9cdf-d389-4fa3-a490-8025f035a21a",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.fff63878-32df-4046-b80d-b7e534954ac3",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5278,
    "who_to_charge":"Antônio Mendes Veloso50838301-37b2-47f4-bc92-d33a5228408d",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204f283edf4-4982-4315-ad62-6aa3519d0de9",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.eab10058-040f-4556-a437-cc96f39dad88",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5279,
    "who_to_charge":"Antônio Mendes Veloso0c24b022-7795-44a0-b171-119f091fcad3",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-2048100f724-0082-4dc0-9eb5-62e4969372ea",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.4ce02560-2799-4571-a98a-8f7000303e7b",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5280,
    "who_to_charge":"Antônio Mendes Veloso699f26ba-4da6-4438-908c-a884484f9ed6",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204255560ba-37b0-441c-817f-ac73b6bca36c",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.ac39d5f8-f257-4ab6-8083-cff1f34ca7a4",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5281,
    "who_to_charge":"Antônio Mendes Veloso7b03e7f1-695b-4166-9330-55862cf26e37",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-2042679d244-c86e-4a52-951a-49b640bb8d7c",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.ff9e34b1-20c2-4b53-b9da-c238aca27c56",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5282,
    "who_to_charge":"Antônio Mendes Veloso789ba8ee-2604-4215-80dc-b0d96e1ef4d3",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204dc6130dd-f844-4cd9-b0d8-cc22f89ac7f3",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.004a0874-f93f-4c97-ba14-21d340ab0510",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5283,
    "who_to_charge":"Antônio Mendes Veloso4dda6067-8419-4b99-858d-0b1db3832813",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-20440cf14e9-4926-4868-a93d-a146f04e07fd",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.f9328358-4fd1-4120-a739-1d807059ff6b",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5284,
    "who_to_charge":"Antônio Mendes Veloso7da6f78c-e581-45b8-ad8f-288972875212",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204aa22f9f8-e85c-43cf-8785-767c4da432e7",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.ff725fd6-2be0-4fab-a13d-14fb455e4836",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5285,
    "who_to_charge":"Antônio Mendes Veloso93d0c599-f6d5-4b0f-b90f-0b874087be4c",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204bfd49da6-9824-4d4f-a92d-984b23447fb8",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.189f2581-67c8-4675-9e02-722126f7b401",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5286,
    "who_to_charge":"Antônio Mendes Veloso6ea7766d-25d1-4e4f-97a2-0ca35ab41a82",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-2046933a061-007f-4772-b81b-3c87caf729cb",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.5b51d292-7660-41d1-9008-d7584a0e1b58",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5287,
    "who_to_charge":"Antônio Mendes Veloso9cb3595d-cd2d-441a-b2a7-4980b64c9a56",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-20437b58754-b90e-43ae-b0ba-2cf764e89625",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.1a3ef8e8-cb08-48f2-ace0-bd27368e3441",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5288,
    "who_to_charge":"Antônio Mendes Veloso5c37c6e2-af1e-4133-a231-85363df0fedb",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-2047e614d11-d97f-44b5-be37-da9768272aad",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.8e798ff8-9306-4fc2-8f66-8c2df7fec6b7",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5289,
    "who_to_charge":"Antônio Mendes Velosoc6bc4658-b4b7-4e67-9f8b-be80a54265cf",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204fe7e06c0-fca2-46c7-ba2f-25ab998e76ed",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.f9a41c8b-1664-48cf-b69e-b312ba60250b",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5290,
    "who_to_charge":"Antônio Mendes Veloso2a441a7a-3afa-48d5-ba79-39c88d1bed1f",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204088dc6f0-fd7c-49b5-9a6d-722e349d5891",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.0cc6fbfb-d8e7-4877-9c42-090fde58a22b",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5291,
    "who_to_charge":"Antônio Mendes Velosoc11a33b5-b90f-40b9-846a-6a07801d2c9f",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204896114d3-b2da-49da-9ed0-e901a706599b",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.a982f694-8f6b-4183-8676-e7cb295ab441",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5268,
    "who_to_charge":"Antônio Mendes Veloso31310bca-7ed9-4ab5-9532-ced7ab7e9d9a",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204c4c2e8c5-45a2-4f7e-8b1e-497a94cd7dc3",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.24a1acd8-90bf-488b-becc-37eae651e7de",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5269,
    "who_to_charge":"Antônio Mendes Veloso1d4c36dc-589a-4ddb-b0d7-a9de6f812ff6",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204792f510b-f08c-420e-8d28-c7402bf700b1",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.4b11ff1a-c9af-4bca-aae3-e464d02e5c0b",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5270,
    "who_to_charge":"Antônio Mendes Velosoaeeba528-3945-49c9-bce7-0570609525b4",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204a9d65fb0-c246-4f72-a203-644b327b7702",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.b687cba4-3d82-4c53-9f47-fd547ad55145",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5271,
    "who_to_charge":"Antônio Mendes Velosofe8c20a3-9c1f-4411-ac37-1f0f8d6ca437",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-2042ccf9faf-5d7b-4c01-b752-905d38723d9e",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.406b35af-476e-43d9-a01b-67e05a378f56",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5292,
    "who_to_charge":"Antônio Mendes Veloso9f91a60e-6775-47cb-bb53-917299eab729",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-2045667ba31-a651-4805-8586-ed8297fd05c1",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.cee282f6-55d5-4297-82be-05a86c33708e",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5293,
    "who_to_charge":"Antônio Mendes Velosoa08f22ce-3489-4ad8-95fc-a6bda0af2f55",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-2045cfe76db-9cd9-4c6f-a623-d7aeb3ecad2e",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.f872bb1d-03b6-4003-9352-7a29e290ede6",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5294,
    "who_to_charge":"Antônio Mendes Velosoe157f213-e87b-478d-ba50-835db047ab1d",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204e111f369-4727-4063-b8fa-47b527a508e3",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.2e2634c5-0b4d-4f92-99c1-997025c75ff1",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5295,
    "who_to_charge":"Antônio Mendes Veloso4664106f-d374-4088-905d-9c08a6feb25d",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204d6a0707e-b3b3-458a-9cb6-a8c2279adcbe",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.9aaaf3fb-53fb-4ed8-a5ff-242275afd9cb",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5296,
    "who_to_charge":"Antônio Mendes Velosob9b8ba41-1232-4267-adc0-883dadc3a468",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-20461897b1d-e528-4450-a3c3-22e566353476",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.d76a90d9-9f51-4f23-977a-fa3094bead37",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5297,
    "who_to_charge":"Antônio Mendes Velosob5a15011-0977-454b-b63d-f1dbc62d4b69",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-2046068d55d-1e7a-456a-88b0-f0cdb18b6eca",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.113ceaa6-27a9-460a-9b72-8bab936f50c0",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5298,
    "who_to_charge":"Antônio Mendes Veloso9dfc8659-90ec-4532-a763-191f1d151855",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-20409241286-d926-422d-b8c0-80fc1cf13647",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.28e5f49a-95a4-4518-a71b-31544e461d47",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5299,
    "who_to_charge":"Antônio Mendes Velosoed0f136d-a6f7-451f-9238-bc1f6e6cca61",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-20447b4a8c0-fb9f-4679-a790-7a2188c98975",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.e69b9555-4bb1-42e9-87e5-a0ff80497127",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5300,
    "who_to_charge":"Antônio Mendes Veloso0c75f92a-2fda-4bfc-a0ac-3ccbd8096338",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-2044d745565-dea2-42d1-8347-9c215149ba15",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.1469acf1-4ea7-4125-a26a-acb0e4626105",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5301,
    "who_to_charge":"Antônio Mendes Velosof65098c3-3e25-45e6-8ffc-4813bebfca01",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204a3fdcce2-42e1-4657-a21f-329c7a2a55aa",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.8d58009d-7766-4b57-993b-cf3ead5afb5d",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5302,
    "who_to_charge":"Antônio Mendes Velosofc488fb2-369c-46af-a12b-b45e4bbceee9",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-20414febf44-886a-4965-b277-5d9e40cd2aa5",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.540ac1cc-fc92-43c7-914a-3c78b2589366",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5303,
    "who_to_charge":"Antônio Mendes Veloso9983c725-77c4-4635-89ac-6969ebe52a43",
    "address":"Casa 9 s/n Marginal Luana Lessa, Porangaba, CE 82651-204d6f7d5f4-a2c3-45da-b16a-f8e5bb1b0ec6",
    "phone1":"(15) 98475-4057",
    "description":"Tempora similique nam. Aut quia quia. Qui et omnis.16a042c2-ad4b-47b5-8dd2-2f1193e65331",
    "value":250.0,
    "paid":false,
    "charge_at":"01/01/2012",
    "created_at":"02/03/2019"
  },
  {
    "id":5261,
    "who_to_charge":"Lucas Gabriel Oliveira Velasques",
    "address":"5031 Rua Maria Clara Campos, Espigão Alto do Iguaçu, RR 74614-695",
    "phone1":"(68) 92492-9210",
    "description":"Vero ea ducimus. Commodi rerum sapiente. Maxime officia sit.",
    "value":250.0,
    "paid":false,
    "charge_at":"27/02/2019",
    "created_at":"27/02/2019"
  },
  {
    "id":5263,
    "who_to_charge":"Luiz Henrique Corrêa da Terra",
    "address":"3790 Rua Márcio Espinheira, Buriti dos Lopes, AP 62151-900",
    "phone1":"(15) 91236-7646",
    "description":"Quae expedita beatae. Nesciunt dolore excepturi. Sapiente atque saepe.",
    "value":250.0,
    "paid":false,
    "charge_at":"27/02/2019",
    "created_at":"27/02/2019"
  }
];

export default function pendingCharges(state = initialPendingChargesData, action) {
  switch (action.type) {
    case 'INCREMENT':
      return state + 1
    case 'DECREMENT':
      return state - 1
    default:
      return state
  }
}