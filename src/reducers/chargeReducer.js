// Reducer para tratar os dados de uma Charge/Cobrança
const initialChargeData = {
  chargeData: {
    id: 5264,
    who_to_charge: "Esther Munhoz Pires",
    address: "328 Marginal Talita, Bom Retiro, PE 89897-254",
    phone1: "(66) 94686-5130",
    phone2: "(77) 94686-5130",
    description: "Quos vel voluptatum. Voluptatum eaque occaecati. Est officiis eum.",
    value: 250.0,
    paid: true,
    charge_at: "27/02/2019",
    created_at: "27/02/2019"
  }
  
}

export default function charge(state = initialChargeData, action) {
  switch (action.type) {
    // case "ADD":
    //   ...
    default:
      return state
  }
}