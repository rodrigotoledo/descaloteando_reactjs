// Reducer de Lista de Charges/Cobranças PENDENTES
const initialPaidChargesData =[
  {  
    "id":5262,
    "who_to_charge":"Lucas Gabriel Saraiva da Mata",
    "address":"s/n Rodovia Miguel Marques, Deputado Irapuan Pinheiro, AP 83484-550",
    "phone1":"(35) 98029-1277",
    "description":"Odit vero consequatur. Cum eius odit. Aut deserunt est.",
    "value":250.0,
    "paid":true,
    "charge_at":"27/02/2019",
    "created_at":"27/02/2019"
  },
  {  
    "id":5264,
    "who_to_charge":"Esther Munhoz Pires",
    "address":"328 Marginal Talita, Bom Retiro, PE 89897-254",
    "phone1":"(66) 94686-5130",
    "description":"Quos vel voluptatum. Voluptatum eaque occaecati. Est officiis eum.",
    "value":250.0,
    "paid":true,
    "charge_at":"27/02/2019",
    "created_at":"27/02/2019"
  }
];

export default function paidCharges(state = initialPaidChargesData, action) {
  switch (action.type) {
    case 'INCREMENT':
      return state + 1
    case 'DECREMENT':
      return state - 1
    default:
      return state
  }
}