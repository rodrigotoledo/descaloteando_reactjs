import { combineReducers } from 'redux';

import pendingCharges from './pendingChargesReducer';
import paidCharges from './paidChargesReducer';
import counterCharges from './counterChargesReducer';
import charge from './chargeReducer';
import user from './userReducer';

export default combineReducers({
  pendingCharges, paidCharges, counterCharges, charge, user
})