import React from 'react';

import './style.css';
import ChargesTable from '../../components/ChargesTable';

const SectionCharges = (props) => (
  <section className="section">
    <div className="section-heading">
      <h3 className="title is-2">{props.title}</h3>
      <h4 className="subtitle is-5">{props.charges.length} {props.subtitle}</h4>
    </div>
    <div>
      <ChargesTable charges={props.charges} />
    </div>
  </section>
);

export default SectionCharges;