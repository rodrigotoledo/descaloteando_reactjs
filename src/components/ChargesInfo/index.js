import React from 'react';

import './style.css';

import { connect } from 'react-redux';

const mapStateToProps = stateReducer => ({
  pendingCharges: stateReducer["pendingCharges"],
  paidCharges: stateReducer["paidCharges"],
  counterCharges: stateReducer["counterCharges"]
});

const ChargesInfo = ({ pendingCharges, paidCharges, counterCharges }) => (
  <div>
    <h3 className='title is-4'>Números atuais</h3>
    <h4 className='subtitle is-5'>Acompanhe em tempo real sua(s) {pendingCharges.length + paidCharges.length} cobranças</h4>
    <div className='content' id='charges_informations'>
      <article className='media'>
        <div className='media-content'>
          <div className='content'>
            <p>
              <strong>Cobranças em dia {paidCharges.length}:</strong>
              <br />
              <progress className='progress is-primary' max={pendingCharges.length + paidCharges.length} value={paidCharges.length}></progress>
            </p>
          </div>
        </div>
      </article>
      <article className='media'>
        <div className='media-content'>
          <div className='content'>
            <p>
              <strong>Cobranças pendentes {pendingCharges.length}:</strong>
              <br />
              <progress className='progress is-primary' max={pendingCharges.length + paidCharges.length} value={pendingCharges.length}></progress>
            </p>
          </div>
        </div>
      </article>
      <article className='media'>
        <div className='media-content'>
          <div className='content'>
            <p>
              <strong>Pendentes em até duas semanas {counterCharges.twoWeeksAgo}:</strong>
              <br />
              <progress className='progress is-primary' max={pendingCharges.length} value={counterCharges.twoWeeksAgo}></progress>
            </p>
          </div>
        </div>
      </article>
      <article className='media'>
        <div className='media-content'>
          <div className='content'>
            <p>
              <strong>Pendentes com mais de duas semanas {counterCharges.twoWeeksMore}:</strong>
              <br />
              <progress className='progress is-primary' max={pendingCharges.length} value={counterCharges.twoWeeksMore}></progress>
            </p>
          </div>
        </div>
      </article>
    </div>
  </div>
)

export default connect(mapStateToProps)(ChargesInfo);