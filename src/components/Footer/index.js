import React from 'react';

import './style.css';

const Footer = () => (
  <footer className="footer">
    <div className="section-heading">
      <p>
        <strong>Descaloteando - rodrigo@rtoledo.inf.br</strong>
      </p>
    </div>
  </footer>
);

export default Footer;

