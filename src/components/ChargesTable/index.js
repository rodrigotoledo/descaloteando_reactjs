import React from 'react';

import './style.css';

const ChargesTable = (props) => (
  <table className="table table is-striped is-hoverable is-fullwidth">
    <thead>
      <tr>
        <th abbr="ID">ID</th>
        <th abbr="Cliente/Empresa">Cliente/Empresa</th>
        <th abbr="Telefone">Telefone</th>
        <th abbr="Endereço">Endereço</th>
        <th abbr="Valor">Valor</th>
        <th abbr="Criada em">Criada em</th>
        <th abbr="Cobrar em">Cobrar em</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      {props.charges.map(charge => (
        <tr key={charge['id']}>
          <td>#{charge['id']}</td>
          <td>{charge['who_to_charge']}</td>
          <td>{charge['phone1']}</td>
          <td>{charge['address']}</td>
          <td>{charge['value']}</td>
          <td>{charge['created_at']}</td>
          <td>{charge['charge_at']}</td>
          <td className="actions">
          <div className="buttons has-addons">
            <a className="button is-small is-danger" href="/">Editar</a>
            <a className="button is-small is-link" href="/">Apagar</a>
          </div>
          </td>
        </tr>
      ))}
    </tbody>
  </table>
);

export default ChargesTable;
