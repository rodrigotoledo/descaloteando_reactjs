import React from 'react';

import './style.css';

import { connect } from 'react-redux';

const mapStateToProps = stateReducer => ({
  user: stateReducer["user"]
});

const UserForm = ({ user }) => (
  <div>
    <h3 className='title is-4'>Editar meus dados</h3>
    <h4 className='subtitle is-5'>Preencha os dados abaixo para alterar suas informações</h4>
    <div className='columns is-centered'>
      <div className='column'>
        <form action="/users" acceptCharset="UTF-8" data-remote="true" method="post">
          <div className='field'>
            <input autoComplete="email" className="input is-dark" placeholder="Informe seu email" type="email" name="user[email]" id="user_email" defaultValue={user.userData["email"]} />
          </div>
          <div className='field'>
            <div className='field-body'>
              <div className='field'>
                <input autoComplete="new-password" className="input is-dark" placeholder="Informe sua senha" type="password" name="user[password]" id="user_password" />
              </div>
              <div className='field'>
                <input autoComplete="new-password" className="input is-dark" placeholder="Confirme sua senha" type="password" name="user[password_confirmation]" id="user_password_confirmation" />
              </div>
            </div>
          </div>
          <div className='field'>
            <input autoComplete="current-password" className="input is-dark" placeholder="Informe sua senha atual" type="password" name="user[current_password]" id="user_current_password" />
            <p className='help is-dark'>
              <i>(Para alterar suas informações é necessário sua senha atual)</i>
            </p>
          </div>
          <div className='field'>
            <div className='field-body'>
              <div className='field'>
                <input className="input is-dark" placeholder="Informe seu nome" type="text" name="user[name]" id="user_name" defaultValue={user.userData["name"]} />
              </div>
              <div className='field'>
                <input className="input is-dark" placeholder="Informe seu telefone" type="text" id="user_phone" defaultValue={user.userData["phone"]} />
              </div>
            </div>
          </div>
          <div className='field'>
            <input className="input is-dark" placeholder="Informe seu endereço" type="text" name="user[address]" id="user_address" defaultValue={user.userData["address"]} />
          </div>
          <div className='field'>
            <div className='control'>
              <div className="buttons has-addons">
                <input type="submit" name="commit" value="Atualizar seus dados" className="button is-small is-link" data-disable-with="Atualizar seus dados" />
                <input data-confirm="Deseja realmente nos deixar??!" className="button is-small is-danger" type="submit" value="Insatisfeito? Cancele sua conta..." />
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    
  </div>
);
  
export default connect(mapStateToProps)(UserForm);