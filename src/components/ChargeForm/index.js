import React from 'react';

import './style.css';

import { connect } from 'react-redux';

const mapStateToProps = stateReducer => ({
  charge: stateReducer["charge"]
});

const ChargeForm = ({ charge }) => (

  <div>
    <h3 className='title is-4'>Formulário de Cobrança</h3>
    <h4 className='subtitle is-5'>Preencha os dados abaixo para salvar a cobrança</h4>
    <div className='columns is-centered'>
      <div className='column'>
        <div className="chargeFormErrors content">
        </div>
        <form action="/charges" acceptCharset="UTF-8" data-remote="true" method="post">
          <div className='field'>
            <div className='control'>
              <input className="input is-dark" required="required" placeholder="Cliente/Empresa" type="text" name="charge[who_to_charge]" id="charge_who_to_charge" defaultValue={charge.chargeData["who_to_charge"]} />
              <p className='help is-dark'>
                Informe para quem esta cobrança está sendo gerada
              </p>
            </div>
          </div>
          <div className='field'>
            <div className='control'>
              <input className="input is-dark" required="required" placeholder="Endereço" type="text" name="charge[address]" id="charge_address" defaultValue={charge.chargeData["address"]}  />
              <p className='help is-dark'>
                Deve ser um endereço válido
              </p>
            </div>
          </div>
          <div className='field'>
            <div className='field-body'>
              <div className='field'>
                <input className="input is-dark" required="required" placeholder="Telefone" type="text" name="charge[phone1]" id="charge_phone1" defaultValue={charge.chargeData["phone1"]} />
                <p className='help is-dark'>
                  Preencha com o celular
                </p>
              </div>
              <div className='field'>
                <input className="input is-dark" required="required" placeholder="Telefone secundário" type="text" name="charge[phone2]" id="charge_phone2" defaultValue={charge.chargeData["phone2"]} />
                <p className='help is-dark'>
                  Preencha com outro telefone
                </p>
              </div>
            </div>
          </div>
          <div className='field'>
            <div className='field-body'>
              <div className='field'>
                <input className="input is-dark" required="required" placeholder="Valor" type="number" name="charge[value]" id="charge_value" defaultValue={charge.chargeData["value"]} />
                <p className='help is-dark'>
                  Preencha com o valor
                </p>
              </div>
              <div className='field'>
                <select id="charge_charge_at_3i" name="charge[charge_at(3i)]" className="input is-dark" placeholder="Data a ser cobrada">
                  <option value="1">01</option>
                  <option value="2">02</option>
                  <option value="3">03</option>
                  <option value="4">04</option>
                  <option value="5">05</option>
                  <option value="6">06</option>
                  <option value="7">07</option>
                  <option value="8">08</option>
                  <option value="9">09</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                  <option value="13">13</option>
                  <option value="14">14</option>
                  <option value="15">15</option>
                  <option value="16">16</option>
                  <option value="17">17</option>
                  <option value="18">18</option>
                  <option value="19">19</option>
                  <option value="20">20</option>
                  <option value="21">21</option>
                  <option value="22">22</option>
                  <option value="23">23</option>
                  <option value="24">24</option>
                  <option value="25">25</option>
                  <option value="26">26</option>
                  <option value="27">27</option>
                  <option value="28" defaultValue="selected">28</option>
                  <option value="29">29</option>
                  <option value="30">30</option>
                  <option value="31">31</option>
                </select>
                <select id="charge_charge_at_2i" name="charge[charge_at(2i)]" className="input is-dark" placeholder="Data a ser cobrada">
                  <option value="1">01</option>
                  <option value="2" defaultValue="selected">02</option>
                  <option value="3">03</option>
                  <option value="4">04</option>
                  <option value="5">05</option>
                  <option value="6">06</option>
                  <option value="7">07</option>
                  <option value="8">08</option>
                  <option value="9">09</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                </select>
                <select id="charge_charge_at_1i" name="charge[charge_at(1i)]" className="input is-dark" placeholder="Data a ser cobrada">
                  <option value="2014">2014</option>
                  <option value="2015">2015</option>
                  <option value="2016">2016</option>
                  <option value="2017">2017</option>
                  <option value="2018">2018</option>
                  <option value="2019" defaultValue="selected">2019</option>
                  <option value="2020">2020</option>
                  <option value="2021">2021</option>
                  <option value="2022">2022</option>
                  <option value="2023">2023</option>
                  <option value="2024">2024</option>
                </select>
                <p className='help is-dark'>
                  Data que foi gerada
                </p>
              </div>
            </div>
          </div>
          <div className='field'>
            <div className='control'>
              <textarea className="textarea is-dark" required="required" placeholder="Descrição" rows="2" name="charge[description]" id="charge_description" defaultValue={charge.chargeData["description"]}></textarea>
              <p className='help is-dark'>
                Informe a descrição
              </p>
            </div>
          </div>
          <div className='field'>
            <div className='control'>
              <p className='help is-dark'>
                <input type="checkbox" defaultValue="1" name="charge[paid]" id="charge_paid" defaultChecked={charge.chargeData["paid"]} />
                Pagamento realizado?
              </p>
            </div>
          </div>
          <div className='field is-grouped has-text-centered actions'>
            <div className='control buttons has-addons'>
              <input type="button" onClick={() => {}} name="commit" value="Salvar" className="button is-small is-link" data-disable-with="Salvar" />
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
);

export default connect(mapStateToProps)(ChargeForm);