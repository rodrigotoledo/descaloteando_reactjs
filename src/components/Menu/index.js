import React from 'react';
import { BrowserRouter as Router, Link } from "react-router-dom";

import './style.css';

const Menu = () => (
  <nav className="navbar is-link is-fixed-top">
    <div className="navbar-brand">
      <div className="navbar-burger burger" data-target="navbar-transparent">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
    <div className="navbar-menu" id="navbar-transparent">
      <div className="navbar-end">
        <a className="navbar-item" href="#about">
          <span className="icon">
            <i className="fas fa-info"></i>
          </span>
          <span>Números atuais</span>
        </a>
        <a className="navbar-item" href="#pending_charges">
          <span className="icon">
            <i className="fas fa-th-list"></i>
          </span>
          <span>Cobranças atrasadas</span>
        </a>
        <a className="navbar-item" href="#paid_charges">
          <span className="icon">
            <i className="fas fa-th-list"></i>
          </span>
          <span>Cobranças em dia</span>
        </a>
        <a className="navbar-item" href="#about">
          <span className="icon">
            <i className="fas fa-plus-square"></i>
          </span>
          <span>Criar Cobrança</span>
        </a>
        <a className="navbar-item" href="#about">
          <span className="icon">
            <i className="fas fa-user-edit"></i>
          </span>
          <span>Editar meus dados</span>
        </a>
        <Link to="/sign-in" className="navbar-item">
          <span className="icon">
            <i className="fas fa-sign-out-alt"></i>
          </span>
          <span>Sair</span>
        </Link>
      </div>
    </div>
  </nav>

);

export default Menu;