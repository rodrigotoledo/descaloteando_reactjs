import React from 'react'

const SectionHeader = () => (
  <section className="hero is-link">
    <div className="hero-body">
      <div className="container">
        <h1 className="title is-1">
          Já se sentiu levando um calote?
        </h1>
        <h2 className="subtitle is-3">
          Para sua empresa, ou para você mesmo... é hora de colocar tudo no controle
        </h2>
        <h1 className="title is-1 has-text-centered">
          <span className="icon">
            <i className="fas fa-people-carry" />
          </span>
          &nbsp;
          DESCALOTEANDO!
        </h1>
      </div>
    </div>
  </section>
)

export default SectionHeader
