## Sobre o projeto

*Este projeto serve apenas para estudo e aprendizado sendo exposto em meu canal do facebook ou youtube

Inicialmente temos o seguinte problema a ser resolvido: **Você** ou sua **Empresa** levam calotes todos os dias por notas promissórias.

Isto é comum em cidades pequenas ou até grandes mesmo.

Como é que então teríamos controle do que é vendido, quando ocorreu, parcelas, inadimplência e principalmente quem estaria atrasando demais e sempre jogando pra frente?!

## Surgiu então a idéia... DESCALOTEANDO!

O projeto é feito para tentar resolver as situações acima e também ajudar a desenvolvedores com dificuldades em Ruby on Rails, ReactJS e ReactNative a criarem seus projetos com boas práticas.

E além disto e muito importante o projeto ser ajudado por pessoas com mais conhecimento nos frameworks citados acima, logo tornando algo global e com diversas boas práticas e feedbacks.

### Informações sobre o desenvolvimento

#### Ruby on Rails

- Versão 5.2.x
- Testes com MiniTest
- Banco de dados PostgreSQL
- Framework com apenas API, apenas views para emails
- RVM para versionamento do Ruby e ambiente das gems

#### ReactJS

- Redux
- Comunicação com API usando AXIOS
